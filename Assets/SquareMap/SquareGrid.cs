﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquareGrid : MonoBehaviour {

    public int chunkCountX =4, chunkCountZ = 3;

    int cellCountX, cellCountZ;

    public SquareGridChunk chunkPrefab;

    public SquareCell squareCellPrefab;

    SquareGridChunk[] chunks;
    SquareCell[] cells;
   

    private void Awake()
    {
       

        cellCountX = chunkCountX * SquareMetrics.chunkSizeX;
        cellCountZ = chunkCountZ * SquareMetrics.chunkSizeZ;
        CreateChunks();
        CreateCells();
    }

    void CreateChunks()
    {
        chunks = new SquareGridChunk[chunkCountX * chunkCountZ];

        for(int z = 0, i = 0; z < chunkCountZ; z++)
        {
            for(int x = 0; x < chunkCountX; x++)
            {
                SquareGridChunk chunk = chunks[i++] = Instantiate(chunkPrefab);
                chunk.transform.SetParent(transform);
            }
        }
    }

    void CreateCells()
    {
        cells = new SquareCell[cellCountX * cellCountZ];

        for(int z = 0, i = 0; z < cellCountZ; z++)
        {
            for(int x = 0; x< cellCountX; x++)
            {
                CreateCell(x, z, i++);
            }
        }
    }

    void CreateCell(int x, int z, int i)
    {
        Vector3 position;
        position.x = x * SquareMetrics.length;
        position.y = 0;
        position.z = z * SquareMetrics.length;

        SquareCell cell = cells[i] = Instantiate<SquareCell>(squareCellPrefab);
        cell.coordinates = new Vector3(x, 0, z);
     //   cell.transform.SetParent(transform, false);
        cell.transform.localPosition = position;
        cell.color = Color.white;

        AddCellToChunk(x, z, cell);
    }

    void AddCellToChunk(int x, int z, SquareCell cell)
    {
        int chunkX = x / SquareMetrics.chunkSizeX;
        int chunkZ = z / SquareMetrics.chunkSizeZ;
        SquareGridChunk chunk = chunks[chunkX + chunkZ * chunkCountX];

        int localX = x - chunkX * SquareMetrics.chunkSizeX;
        int localZ = z - chunkZ * SquareMetrics.chunkSizeZ;

        chunk.AddCell(localX + localZ * SquareMetrics.chunkSizeX, cell);
    }

    public SquareCell GetCell(Vector3 position)
    {
        position = transform.InverseTransformPoint(position);

        float x = position.x / SquareMetrics.length;
        float z = position.z / SquareMetrics.length;
        //int chunkX = (int)(position.x / (SquareMetrics.length * SquareMetrics.chunkSizeX));
        //int chunkZ = (int)(position.z / (SquareMetrics.length * SquareMetrics.chunkSizeZ));


        //SquareGridChunk chunk = chunks[chunkX + chunkZ * chunkCountX];

        //int localX = (int)(position.x/ SquareMetrics.length) - chunkX * SquareMetrics.chunkSizeX;
        //int localZ = (int)(position.z / SquareMetrics.length) - chunkZ * SquareMetrics.chunkSizeZ;

        int xx = Mathf.RoundToInt(x);
        int zz = Mathf.RoundToInt(z);

        return cells[xx + zz * cellCountX];
    }
   


    // Update is called once per frame
    void Update () {
		
	}
}
