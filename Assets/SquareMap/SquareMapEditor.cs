﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SquareMapEditor : MonoBehaviour {

    public Color[] colors;

    public SquareGrid grid;

    int activeElevation;

    Color activeColor;
    public void SelectColor(int index)
    {
        activeColor = colors[index];
    }

    public void SetElevation(float elevation)
    {
        activeElevation = (int)elevation;
    }

    void Awake()
    {
        SelectColor(0);
    }

    // Update is called once per frame
    void Update () {
        if (
            Input.GetMouseButton(0) &&
            !EventSystem.current.IsPointerOverGameObject()
        )
        {
            HandleInput();
        }
    }


    void HandleInput()
    {
        Ray inputRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(inputRay, out hit))
        {
            EditCell(grid.GetCell(hit.point));
        }
    }

    void EditCell(SquareCell cell)
    {
        if (cell == null)
        {
            Debug.Log("cell is null!");
            return;
        }
        cell.color = activeColor;
        //cell.Elevation = activeElevation;
        
    }
}
