﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class SquareCell : MonoBehaviour
{
    public Vector3 coordinates;
    private Color _color;
    public Color color
    {
        get { return _color; }
        set
        {
            if(_color == value)
            {
                return;
            }
            _color = value;
            Refresh();
        }
    }
    public SquareGridChunk chunk;

    [SerializeField]
    SquareCell[] neighbors;

    public Vector3 Position
    {
        get
        {
            return transform.localPosition;
        }
    }

    void Refresh()
    {
        if(chunk)
            chunk.Refresh();
    }
}

