﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class SquareMesh : MonoBehaviour
{
    static List<Vector3> vertices = new List<Vector3>();
    static List<Color> colors = new List<Color>();
    static List<int> triangles = new List<int>();

    Mesh squareMesh;
    MeshCollider meshCollider;

    private void Awake()
    {
        GetComponent<MeshFilter>().mesh = squareMesh = new Mesh();
        meshCollider = gameObject.AddComponent<MeshCollider>();
      
        squareMesh.name = "Square Mesh";
    }


    public void Triangulate(SquareCell[] cells)
    {
        squareMesh.Clear();
        vertices.Clear();
        colors.Clear();
        triangles.Clear();
        for (int i = 0; i < cells.Length; i++)
        {
            Triangulate(cells[i]);
        }
        squareMesh.vertices = vertices.ToArray();
        squareMesh.colors = colors.ToArray();
        squareMesh.triangles = triangles.ToArray();
        squareMesh.RecalculateNormals();
        meshCollider.sharedMesh = squareMesh;
    }

    void Triangulate(SquareCell cell)
    {
        Vector3 center = cell.Position;
        Vector3 v1 = new Vector3(center.x - SquareMetrics.halfLen, 0, center.z + SquareMetrics.halfLen);
        Vector3 v2 = new Vector3(center.x + SquareMetrics.halfLen, 0, center.z + SquareMetrics.halfLen);
        Vector3 v3 = new Vector3(center.x + SquareMetrics.halfLen, 0, center.z - SquareMetrics.halfLen);
        Vector3 v4 = new Vector3(center.x - SquareMetrics.halfLen, 0, center.z - SquareMetrics.halfLen);
        AddQuadUnperturbed(v2, v1, v3, v4);
        AddQuadColor(cell.color, cell.color);
    }

    public void AddTriangle(Vector3 v1, Vector3 v2, Vector3 v3)
    {
        int vertexIndex = vertices.Count;
        vertices.Add((v1));
        vertices.Add(v2);
        vertices.Add((v3));
        triangles.Add(vertexIndex);
        triangles.Add(vertexIndex + 1);
        triangles.Add(vertexIndex + 2);
    }

    public void AddTriangleUnperturbed(Vector3 v1, Vector3 v2, Vector3 v3)
    {
        int vertexIndex = vertices.Count;
        vertices.Add(v1);
        vertices.Add(v2);
        vertices.Add(v3);
        triangles.Add(vertexIndex);
        triangles.Add(vertexIndex + 1);
        triangles.Add(vertexIndex + 2);
    }

    public void AddQuad(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4)
    {
        int vertexIndex = vertices.Count;
        vertices.Add((v1));
        vertices.Add((v2));
        vertices.Add((v3));
        vertices.Add((v4));
        triangles.Add(vertexIndex);
        triangles.Add(vertexIndex + 2);
        triangles.Add(vertexIndex + 1);
        triangles.Add(vertexIndex + 1);
        triangles.Add(vertexIndex + 2);
        triangles.Add(vertexIndex + 3);
    }

    public void AddQuadUnperturbed(
    Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4
)
    {
        int vertexIndex = vertices.Count;
        vertices.Add(v1);
        vertices.Add(v2);
        vertices.Add(v3);
        vertices.Add(v4);
        triangles.Add(vertexIndex);
        triangles.Add(vertexIndex + 2);
        triangles.Add(vertexIndex + 1);
        triangles.Add(vertexIndex + 1);
        triangles.Add(vertexIndex + 2);
        triangles.Add(vertexIndex + 3);
    }

    void AddTriangleColor(Color color)
    {
        colors.Add(color);
        colors.Add(color);
        colors.Add(color);
    }

    void AddTriangleColor(Color c1, Color c2, Color c3)
    {
        colors.Add(c1);
        colors.Add(c2);
        colors.Add(c3);
    }

    void AddQuadColor(Color c1, Color c2)
    {
        colors.Add(c1);
        colors.Add(c1);
        colors.Add(c2);
        colors.Add(c2);
    }

    void AddQuadColor(Color c1, Color c2, Color c3, Color c4)
    {
        colors.Add(c1);
        colors.Add(c2);
        colors.Add(c3);
        colors.Add(c4);
    }
}

