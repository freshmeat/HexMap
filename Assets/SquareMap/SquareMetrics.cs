﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class SquareMetrics
{
    public const float length = 1f;
    public const float halfLen = length * 0.5f;

    public const int chunkSizeX = 5, chunkSizeZ = 5;
}

