﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class SquareGridChunk : MonoBehaviour
{
    SquareCell[] cells;

    SquareMesh sMesh;

    private void Awake()
    {
        sMesh = GetComponentInChildren<SquareMesh>();
        cells = new SquareCell[SquareMetrics.chunkSizeX * SquareMetrics.chunkSizeZ];
    }



    public void AddCell(int index, SquareCell cell)
    {
        cells[index] = cell;
        cell.chunk = this;
        cell.transform.SetParent(transform, false);
       // cell.uiRect.SetParent(gridCanvas.transform, false);
    }

    public void Refresh()
    {
        enabled = true;
       
    }

    private void LateUpdate()
    {
        sMesh.Triangulate(cells);
        enabled = false;
    }
}

